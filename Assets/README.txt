READ ME!
The project(Inventory System) is developed using Unity 2019.2.21f1

Project structure Folders

art assets
-General art files for icon etc.

Editor
-It has the editor scripts which are used to create an Inventory item tool.

Prefabs
-Only contains the store item prefab which is scrollable.

Scenes
-SampleScene(it is the scene which has the content)

Scripts(folder)

-EventsFolder
Created the generic event system classes which are static, can create events specific for various class or events.

-Inventory FolderInventoryItems.cs
This folder contains the InventoryItems scriptable, which is used as a singleton database for the whole game.
Users add values from the Editor tool to this scriptable DB.
This is also into the editor format, just for better visualizations and organization.
The Scriptable created from this is placed in the resources folder,(Just did that because we are using Singelton format of this data)

InventortItem.cs It is the script which is applied item prefab, it handles the details for each store item.

UIScriptsFolder
-This folder contains 2 scripts
GUIManager.cs
This script is kind of which handles the IN Game GUI structure, so at the moment we are just playing with the items like displaying store screen
and displaying the number of coins and hearts from this code.

StoreGUIManager.cs
It handles the complete storeScreenPanel.
features it has
=Display dynamic scroller for items.
=Display the details of each store item on click.
=You can sell items from this code.
=Increase and decrease the quality of selling items.

MiscelleneousFolder
It has 2 scripts
RewardTimer.cs
-these scripts keep the record of a time when the application goes pause or quit.
-It is generic one TimerEvents can be used to send reward after some time via this script.


SaveDataManager.cs(Not at very advance level)
-Used Serialization to store the data of InventoryItems such as their quantity and their availability.