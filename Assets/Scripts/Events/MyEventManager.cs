﻿
public class MyEventManager
{
    public static bool HasInstance = true;

    public static class InventoryEvents
    {
        public static MyEvent<InventoryItem, ItemDetails> OnItemSelected = new MyEvent<InventoryItem, ItemDetails>();

    }

    public static class TimerEvents
    {
        public static MyEvent<int> OnRewardActivate = new MyEvent<int>();
    }
}
