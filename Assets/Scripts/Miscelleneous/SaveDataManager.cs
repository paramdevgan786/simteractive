﻿using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

[System.Serializable]
public class GameData
{
    public int TotalCoins;
    public int TotalHearts;
    public List<StoreItem> storeItems;

}

[System.Serializable]
public class StoreItem
{
    public string Name;
    public int Quantity;
    public int SellPrice;
    public bool isSoldOut;

}
public class SaveDataManager : MonoBehaviour
{
    private GameData gameData;
    private string DataPath = "/GameData.gd";
    // Start is called before the first frame update
    private void OnEnable()
    {
        ReterieveData();
    }

    private void ReterieveData()
    {
        gameData = new GameData();

        if (File.Exists(Application.persistentDataPath + DataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + DataPath, FileMode.Open);
            gameData = (GameData)bf.Deserialize(file);
            int itemsCount = gameData.storeItems.Count;

            for (int ctn = 0; ctn < InventoryItems.Instance.items.Count; ctn++)
            {
                for (int index = 0; index < itemsCount; index++)
                {
                    if (InventoryItems.Instance.items[index].ItemName.Contains(gameData.storeItems[index].Name))
                    {
                        InventoryItems.Instance.items[index].StartingQuantity = gameData.storeItems[index].Quantity;
                        InventoryItems.Instance.items[index].SellPrice = gameData.storeItems[index].SellPrice;
                        InventoryItems.Instance.items[index].isSoldOut = gameData.storeItems[index].isSoldOut;
                    }

                }
            }

            InventoryItems.Instance.TotalCoins = gameData.TotalCoins;
            InventoryItems.Instance.TotalHearts = gameData.TotalHearts;

            GUIManager.Instance.UpdateDefaultCurrencyAmount();
            file.Close();

        }
    }


    private void SaveData()
    {
        gameData = new GameData();
        gameData.TotalCoins = InventoryItems.Instance.TotalCoins;
        gameData.TotalHearts = InventoryItems.Instance.TotalHearts;
        gameData.storeItems = new List<StoreItem>();

        int itemsCount = InventoryItems.Instance.items.Count;
        for (int index = 0; index < itemsCount; index++)
        {

            StoreItem storeItem = new StoreItem();
            storeItem.Name = InventoryItems.Instance.items[index].ItemName;
            storeItem.Quantity = InventoryItems.Instance.items[index].StartingQuantity;
            storeItem.SellPrice = InventoryItems.Instance.items[index].SellPrice;
            storeItem.isSoldOut = InventoryItems.Instance.items[index].isSoldOut;
            gameData.storeItems.Add(storeItem);
        }


        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create(Application.persistentDataPath + DataPath); //you can call it anything you want
        bf.Serialize(file, gameData);
        file.Close();

    }

    private void OnDisable()
    {
        SaveData();
    }

    private void OnApplicationFocus(bool isGameFocus)
    {
        if (!isGameFocus)
        {
            SaveData();
        }
    }

}

