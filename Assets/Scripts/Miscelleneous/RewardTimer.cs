﻿using System.Collections;
using UnityEngine;
using System;

public class RewardTimer : MonoBehaviour
{
    private int tickerCount;
    //set reward interval 
    private int rewardDelay = 5;
    //store last time
    private DateTime lastTime;
    //diffence between last time and right now
    private double differenceInSeconds;
    //the whole value after dividing the differnce with 5 sec delay
    private int completeIntervals;
    //the value after getting the modulas
    private int partialInterval;
    // since focus works at the beginning too, we need to start the focus from second time(just storing count onAppfocus)
    private int focusCount;

    private void Start()
    {
       // PlayerPrefs.DeleteAll();
        Application.runInBackground = true;
        calculateSavedTime();
        StartCoroutine(StartCounter());
    }

    private void calculateSavedTime()
    {
        if (PlayerPrefs.HasKey("TimeString"))
        {
            long tempTime = Convert.ToInt64(PlayerPrefs.GetString("TimeString"));
            DateTime lastDate = DateTime.FromBinary(tempTime);
            calculateInterval(lastDate);
        }
    }

    private IEnumerator StartCounter()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            tickerCount += 1;
            if (tickerCount % rewardDelay == 0)
            {
                MyEventManager.TimerEvents.OnRewardActivate.Dispatch(1);
            }
        }
    }

    private void OnApplicationFocus(bool isGameFocus)
    {
        if (isGameFocus)
        {
            if (focusCount > 0)
            {
                calculateInterval(lastTime);
            }
            focusCount += 1;
        }
        else
        {
            saveCurrentTime();    
        }
    } 

    private void saveCurrentTime()
    {
        lastTime = DateTime.Now;
        PlayerPrefs.SetString("TimeString", lastTime.ToBinary().ToString());
    }

    private void calculateInterval(DateTime lastDateTime)
    {
        differenceInSeconds = (DateTime.Now - lastDateTime).TotalSeconds;
        int timeDiffernce = (Int32)differenceInSeconds;
        
        completeIntervals = timeDiffernce / rewardDelay;
        if (completeIntervals > 0)
        {
            MyEventManager.TimerEvents.OnRewardActivate.Dispatch(completeIntervals);
        }
        partialInterval = timeDiffernce % rewardDelay;
        tickerCount += partialInterval;

    }

}
