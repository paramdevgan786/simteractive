﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryScriptable", menuName = "InventoryItem")]
public class InventoryItems : ScriptableObject
{

    private static InventoryItems instance;

    public static InventoryItems Instance
    {
        get
        {
            if (instance == null)
                instance = Resources.Load<InventoryItems>("InventoryItem");
            return instance;
        }
    }

    public List<ItemDetails>  items= new List<ItemDetails>();
    [SerializeField]
    public int TotalCoins = 500;//500 is the default amount
    [SerializeField]
    public int TotalHearts = 500;//here too
  
}

[System.Serializable]
public class ItemDetails
{

    [Header("The Item Icon")]
    public Sprite Icon;
    [Header("The Item Name")]
    public string ItemName;
    [Header("The Sell Price")]
    public int SellPrice;
    [Header("The Starting Quantity ")]
    public int StartingQuantity;
    [Header("The Sell Currency Type")]
    public CurrencyType currencyType;
    [Header("The Item Currency Icon")]
    public Sprite currencyIcon;
    [Header("To Manage data base")]
    public bool isSoldOut;

}

public enum CurrencyType
{
    Coins,
    Hearts
}



