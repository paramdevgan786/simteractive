﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct InventoryItemStruct
{
    public Text Name;
    public Text Quantity;
    public Image Icon;
    public Button SelectBtn;
}


public class InventoryItem : MonoBehaviour
{
    #region private vars
    //temp class var to store ref of copy
    private ItemDetails itemDetails;
    [SerializeField]
    private InventoryItemStruct itemUIDetails;
    #endregion

    // Start is called before the first frame update
    private void Start()
    {
        itemUIDetails.SelectBtn.onClick.AddListener(ItemClicked);
    }

    private void ItemClicked()
    {
        MyEventManager.InventoryEvents.OnItemSelected.Dispatch(this, itemDetails);
    }

    public void SetValues(ItemDetails details)
    {
        itemDetails = details;
        itemUIDetails.Name.text = itemDetails.ItemName;
        itemUIDetails.Icon.sprite = itemDetails.Icon;
        itemUIDetails.Icon.SetNativeSize();
        itemUIDetails.Quantity.text = itemDetails.StartingQuantity + "";
    }

    public void UpdateQuantityInfo()
    {
        itemUIDetails.Quantity.text = itemDetails.StartingQuantity + "";
    }

}
