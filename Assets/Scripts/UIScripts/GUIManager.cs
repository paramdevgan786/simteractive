﻿using UnityEngine;
using TMPro;

public class GUIManager : MySingleton<GUIManager>
{
    #region private vars
    [Header("InGame Hud Elements")]
    [SerializeField]
    private TextMeshProUGUI coins;
    [SerializeField]
    private TextMeshProUGUI hearts;
    [SerializeField]
    private Transform storePanel;
    //ref to store the default amount
    private int coinsAmount = 500;
    //ref to store the default hearts amount
    private int heartsAmount = 500;
    #endregion

    #region public vars
    [Header("Hearts Replenish Quantity")]
    public int RewardAmount;
    #endregion
    private void OnEnable()
    {
        MyEventManager.TimerEvents.OnRewardActivate.AddListener(RecieveReward);
    }

    private void OnDisable()
    {
        MyEventManager.TimerEvents.OnRewardActivate.RemoveListener(RecieveReward);
    }

    // Start is called before the first frame update
    private void Start()
    {
        storePanel.gameObject.SetActive(false);
    }

    public void OpenStorePanel()
    {
        storePanel.gameObject.SetActive(true);
    }

    public void CloseStorePanel()
    {
        storePanel.gameObject.SetActive(false);
    }


    //================================Update Game Currency========================//
    public void UpdateDefaultCurrencyAmount()
    {
        coinsAmount = InventoryItems.Instance.TotalCoins; UpdateCoins(0);
        heartsAmount = InventoryItems.Instance.TotalHearts; UpdateHearts(0);
    }


    public void UpdateCurrency(CurrencyType currencyType, int amount)
    {
        if (currencyType == CurrencyType.Coins)
        {
            UpdateCoins(amount);
        }
        else
        {
            UpdateHearts(amount);
        }
    }


    public void UpdateCoins(int amount)
    {
        coinsAmount += amount;
        coins.text = coinsAmount + "";
        InventoryItems.Instance.TotalCoins = coinsAmount;
    }

    public void UpdateHearts(int amount)
    {
        heartsAmount += amount;
        hearts.text = heartsAmount + "";
        InventoryItems.Instance.TotalHearts = heartsAmount;
    }

    private void RecieveReward(int gapTurns)//5 sec gap iterations
    {
        int reAmount = (RewardAmount * gapTurns);
        UpdateHearts(reAmount);
    }
    //============================================================================//

}
