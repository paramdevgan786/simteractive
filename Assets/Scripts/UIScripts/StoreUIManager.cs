﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class SellItemAttributes
{
    public TextMeshProUGUI itemQuantity;
    public TextMeshProUGUI itemName;
    public Image itemIcon;
    public Image itemCurrencyIcon;
    public TextMeshProUGUI itemCurrencyAmount;//per unit
}


public class StoreUIManager : MonoBehaviour
{
    #region private vars
    [Header("SellPanel Elements")]
    [SerializeField]
    private GameObject sellDetailsPanel;
    [SerializeField]
    private GameObject sellInfoPanel;
    [SerializeField]
    private SellItemAttributes sellItemAttributes;


    [Header("Scrollable Elements")]
    [SerializeField]
    private GameObject scrollContent;
    [SerializeField]
    private GameObject scrollPrefab;


    [Header("Popup Window Elements")]
    [SerializeField]
    private TextMeshProUGUI popUpWindowTitle;
    [SerializeField]
    private GameObject popUpWindow;

    //REFRENCES
    //temp ref to store the max quantity of this item
    private int maxQuantity;
    //ref to store the current quantity value (used when player sells anything)
    private int currentQuantity = 0;
    //temp class var to store ref of selected item
    private ItemDetails selectedItemDetails;
    //the select item REF as whole
    private InventoryItem selectedInventoryItem;
    #endregion

    // Start is called before the first frame update
    private void OnEnable()
    {
        MyEventManager.InventoryEvents.OnItemSelected.AddListener(ReceieveItemSelectionEvent);
    }

    private void OnDisable()
    {
        MyEventManager.InventoryEvents.OnItemSelected.RemoveListener(ReceieveItemSelectionEvent);
    }

    private void Start()
    {
        CreateItems();
        showSellPanel(false);
    }

    private void CreateItems()
    {
        int totalItems = InventoryItems.Instance.items.Count;

        for (int index = 0; index < totalItems; index++)
        {
            if (!InventoryItems.Instance.items[index].isSoldOut)
            {
                GameObject gobj = Instantiate(scrollPrefab);
                gobj.GetComponent<InventoryItem>().SetValues(InventoryItems.Instance.items[index]);
                gobj.transform.SetParent(scrollContent.transform, false);
            }
        }
    }

    private void ReceieveItemSelectionEvent(InventoryItem InventoryItem, ItemDetails itemDetail)
    {
        selectedInventoryItem = InventoryItem;

        selectedItemDetails = itemDetail;

        updateSellInformation();
    }

    private void updateSellInformation()
    {
        sellItemAttributes.itemIcon.sprite = selectedItemDetails.Icon;
        sellItemAttributes.itemName.text = selectedItemDetails.ItemName;
        currentQuantity = maxQuantity = selectedItemDetails.StartingQuantity;
        sellItemAttributes.itemQuantity.text = selectedItemDetails.StartingQuantity + "";
        sellItemAttributes.itemCurrencyIcon.sprite = selectedItemDetails.currencyIcon;
        sellItemAttributes.itemCurrencyAmount.text = (selectedItemDetails.StartingQuantity * selectedItemDetails.SellPrice) + "";
        showSellPanel(true);

    }

    private void showSellPanel(bool show)
    {
        sellInfoPanel.SetActive(!show);
        sellDetailsPanel.SetActive(show);
    }
    //===========================ENABLE/ DIABLE SELL SCREEN ====================//

    public void CloseStorePanel()
    {
        GUIManager.Instance.CloseStorePanel();
    }


    //================================methods related to selling items===========================//
    /// <summary>
    /// Sell items
    /// </summary>
    public void SellItem()
    {
        selectedItemDetails.StartingQuantity = (maxQuantity - currentQuantity);

        if (selectedItemDetails.StartingQuantity == 0)
        {
            //InventoryItems.Instance.items.Remove(selectedItemDetails);
            selectedItemDetails.isSoldOut = true;
            Destroy(selectedInventoryItem.gameObject);
        }
        else
        {
            selectedInventoryItem.UpdateQuantityInfo();
        }
        popUpWindowTitle.text = "You Sold " + currentQuantity + " " + selectedItemDetails.ItemName + " items";
        showSellPopUpWindow();
        GUIManager.Instance.UpdateCurrency(selectedItemDetails.currencyType,currentQuantity * selectedItemDetails.SellPrice);
    }
    /// <summary>
    /// you can increase value of items
    /// </summary>
    public void IncreaseQuantity()
    {
        if (currentQuantity < maxQuantity)
        {
            currentQuantity += 1;
        }
        sellItemAttributes.itemQuantity.text = currentQuantity + "";
        sellItemAttributes.itemCurrencyAmount.text = (currentQuantity * selectedItemDetails.SellPrice) + "";
    }

    public void DecreaseQuantity()
    {
        if (currentQuantity > 1)
        {
            currentQuantity -= 1;
        }
        sellItemAttributes.itemQuantity.text = currentQuantity + "";
        sellItemAttributes.itemCurrencyAmount.text = (currentQuantity * selectedItemDetails.SellPrice) + "";
    }

    private void showSellPopUpWindow()
    {
        popUpWindow.SetActive(true);
        LeanTween.scale(popUpWindow, new Vector3(.8f, .8f, .8f), 1.25f).setEase(LeanTweenType.easeOutBack).setOnComplete(SellingComplete);
    }

    private void SellingComplete()
    {
        popUpWindow.SetActive(false);
        popUpWindow.transform.localScale = Vector2.zero * .2f;
        showSellPanel(false);
    }
    //================================END OF SELLIN CODE===========================//

}
