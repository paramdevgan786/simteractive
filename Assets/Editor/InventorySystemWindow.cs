﻿using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(InventoryItems))]
public class InventorySystemWindow : Editor
{
  
    // Update is called once per frame
    public override void OnInspectorGUI()
    {

        GUILayout.Label("Default Currency Amount", EditorStyles.boldLabel);
        GUILayout.Space(20);
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        InventoryItems.Instance.TotalCoins = EditorGUILayout.IntField("Total Coins", InventoryItems.Instance.TotalCoins);

        InventoryItems.Instance.TotalHearts = EditorGUILayout.IntField("Total Hearts", InventoryItems.Instance.TotalHearts);
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();


        GUILayout.Label("Inventory Items", EditorStyles.boldLabel);
        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Add Item", GUILayout.Width(200)))
        {
            AddItem();
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        for (int index = 0; index < InventoryItems.Instance.items.Count; index++)
        {
            GUILayout.BeginVertical();

            InventoryItems.Instance.items[index].ItemName = EditorGUILayout.TextField("Item Name", InventoryItems.Instance.items[index].ItemName);

            InventoryItems.Instance.items[index].SellPrice = EditorGUILayout.IntField("Item Sell Price", InventoryItems.Instance.items[index].SellPrice);

            InventoryItems.Instance.items[index].StartingQuantity = EditorGUILayout.IntField("Starting Quantity", InventoryItems.Instance.items[index].StartingQuantity);

            InventoryItems.Instance.items[index].isSoldOut = EditorGUILayout.Toggle("is Sold Out", InventoryItems.Instance.items[index].isSoldOut);

            InventoryItems.Instance.items[index].currencyType = (CurrencyType)EditorGUILayout.EnumPopup("Currency Type", InventoryItems.Instance.items[index].currencyType);

            InventoryItems.Instance.items[index].Icon = EditorGUILayout.ObjectField("Item Icon", InventoryItems.Instance.items[index].Icon, typeof(Sprite), false) as Sprite;

            InventoryItems.Instance.items[index].currencyIcon = EditorGUILayout.ObjectField("Currency Icon", InventoryItems.Instance.items[index].currencyIcon, typeof(Sprite), false) as Sprite;

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Add New", GUILayout.Width(100)))
            {
                AddItem();
            }

            if (GUILayout.Button("Remove Item", GUILayout.Width(100)))
            {
                RemoveItem(index);
                return;
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.Space(20);
        }

        // base.OnInspectorGUI();
    }

    private void AddItem()
    {
        InventoryItems.Instance.items.Add(new ItemDetails());
    }

    private void RemoveItem(int index)
    {
        InventoryItems.Instance.items.RemoveAt(index);
    }
}
