﻿using UnityEngine;
using UnityEditor;
public class InventoryEditor : EditorWindow
{

    private ItemDetails item;
    [MenuItem("Store/Create Inventory")]
    public static void ShowWindow()
    {
        GetWindow(typeof(InventoryEditor));
    }
    private void OnEnable()
    {
        item = new ItemDetails();
    }

    private void OnGUI()
    {
        GUILayout.Label("Inventory Items", EditorStyles.boldLabel);

        GUILayout.Space(20);

        GUILayout.BeginVertical();

        item.ItemName = EditorGUILayout.TextField("Item Name", item.ItemName);

        item.SellPrice = EditorGUILayout.IntField("Item Sell Price", item.SellPrice);

        item.StartingQuantity = EditorGUILayout.IntField("Starting Quantity", item.StartingQuantity);

        item.currencyType = (CurrencyType)EditorGUILayout.EnumPopup("Currency Type", item.currencyType);

        item.Icon = EditorGUILayout.ObjectField("Item Icon", item.Icon, typeof(Sprite), false) as Sprite;

        item.currencyIcon = EditorGUILayout.ObjectField("Currency Icon", item.currencyIcon, typeof(Sprite), false) as Sprite;

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Add Item", GUILayout.Width(200)))
        {
            if (isNothingEmpty())
            {
                AddItem();
                this.ShowNotification(new GUIContent("Item Added Successfully"));
            }
            else
            {
                this.ShowNotification(new GUIContent("Fill All Data"));
              
            }
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

    }

    private bool isNothingEmpty()
    {
        if (string.IsNullOrWhiteSpace(item.ItemName) || string.IsNullOrWhiteSpace(item.StartingQuantity + "") || string.IsNullOrWhiteSpace(item.SellPrice + ""))
        {
           
            return false;
        }

        else if (item.Icon == null || item.currencyIcon == null)
        {
            return false;
        }

        

        return true;
    }

    private void AddItem()
    {
        InventoryItems.Instance.items.Add(item);
        item = new ItemDetails();
    }

}
